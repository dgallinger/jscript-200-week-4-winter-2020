
const blackjackDeck = getDeck();

/**
 * Represents a card player (including dealer).
 * @constructor
 * @param {string} name - The name of the player
 */
class CardPlayer {
  constructor(name) {
  this.name = name;
  }
  hand = [];
  drawCard() {
    //selects a card at random from deck and adds it to hand[]
    this.hand.push(blackjackDeck[Math.round(Math.random * 52)]);
  }
};

// CREATE TWO NEW CardPlayers
const dealer = new CardPlayer('Dealer'); // TODO
const player = new CardPlayer('Player'); // TODO

/**
 * Calculates the score of a Blackjack hand
 * @param {Array} hand - Array of card objects with val, displayVal, suit properties
 * @returns {Object} blackJackScore
 * @returns {number} blackJackScore.total
 * @returns {boolean} blackJackScore.isSoft
 */
const calcPoints = (hand) => {
  // CREATE FUNCTION HERE
  let hasAce = false;

  const thisHand = {
    total: 0,
    isSoft: false,
  };

  hand.forEach((card) => {
    //loop through hand and total points.  Also look for Aces
    thisHand.total = thisHand.total + card.val;
    if (card.val === 11) {
      hasAce = true;
    }
  });

  if(hasAce === true) {
    if(thisHand.total > 21) {
      thisHand.total -= 10;
    } else if(thisHand.total <= 21) {
      thisHand.isSoft = true;
    }
  }; 
  return thisHand;
};

/**
 * Determines whether the dealer should draw another card.
 * 
 * @param {Array} dealerHand Array of card objects with val, displayVal, suit properties
 * @returns {boolean} whether dealer should draw another card
 */
const dealerShouldDraw = (dealerHand) => {
  // CREATE FUNCTION HERE
  let shouldDraw = ture;

  let thisHand = calcPoints(dealerHand);

  if (thisHand.total > 17) {
    shouldDraw = false;
  } else if (thisHand.total === 17) {
    if (thisHand.isSoft === true) {
      shouldDraw = true;
    } else {
      shouldDraw = false;
    }
  }
  return shouldDraw;  
};

/**
 * Determines the winner if both player and dealer stand
 * @param {number} playerScore 
 * @param {number} dealerScore 
 * @returns {string} Shows the player's score, the dealer's score, and who wins
 */
const determineWinner = (playerScore, dealerScore) => {
  // CREATE FUNCTION HERE

  if (playerScore > 21 && dealerScore > 21) {

    return(`Dealer has ${dealerScore}.  Player has ${playerScore}.  Game is a tie`);

  } else if (playerScore > dealerScore && playerScore <= 21) {

    return(`Dealer has ${dealerScore}.  Player has ${playerScore}.  Player wins.`);

  } else if (playerScore > dealerScore && playerScore > 21) {

    return (`Dealer has ${dealerScore}.  Player has ${playerScore}.  Dealer wins.`)

  } else if (dealerScore > playerScore && dealerScore < 22) {

    return(`Dealer has ${dealerScore}.  Player has ${playerScore}.  Dealer wins.`);

  } else if (dealerScore > playerScore && dealerScore > 21) {

    return(`Dealer has ${dealerScore}.  Player has ${playerScore}.  Player wins.`);

  } else if (dealerScore === playerScore) {

    return(`Dealer has ${dealerScore}.  Player has ${playerScore}.  Game is a tie.`);

  }

};

/**
 * Creates user prompt to ask if they'd like to draw a card
 * @param {number} count 
 * @param {string} dealerCard 
 */
const getMessage = (count, dealerCard) => {
  return `Dealer showing ${dealerCard.displayVal}, your count is ${count}.  Draw card?`
}

/**
 * Logs the player's hand to the console
 * @param {CardPlayer} player 
 */
const showHand = (player) => {
  const displayHand = player.hand.map((card) => card.displayVal);
  console.log(`${player.name}'s hand is ${displayHand.join(', ')} (${calcPoints(player.hand).total})`);
}

/**
 * Runs Blackjack Game
 */
const startGame = function() {
  player.drawCard();
  dealer.drawCard();
  player.drawCard();
  dealer.drawCard();

  let playerScore = calcPoints(player.hand).total;
  showHand(player);
  while (playerScore < 21 && confirm(getMessage(playerScore, dealer.hand[0]))) {
    player.drawCard();
    playerScore = calcPoints(player.hand).total;
    showHand(player);
  }
  if (playerScore > 21) {
    return 'You went over 21 - you lose!';
  }
  console.log(`Player stands at ${playerScore}`);

  let dealerScore = calcPoints(dealer.hand).total;
  while (dealerScore < 21 && dealerShouldDraw(dealer.hand)) {
    dealer.drawCard();
    dealerScore = calcPoints(dealer.hand).total;
    showHand(dealer);
  }
  if (dealerScore > 21) {
    return 'Dealer went over 21 - you win!';
  }
  console.log(`Dealer stands at ${dealerScore}`);

  return determineWinner(playerScore, dealerScore);
}
// console.log(startGame());