/**
 * Returns an array of 52 Cards
 * @returns {Array} deck - a deck of cards
 */


 const getDeck = () => {
   const cardSuits = ['diamonds', 'hearts', 'spades', 'clubs'];
   const faceCards = ['king', 'queen', 'jack'];
   let myDeck = [];

  for (cardSuit of cardSuits) {
    //create cards 1-10
   for (i = 2; i < 11; i++) {
      const card = {
        suit: cardSuit,
        val: i,
        displayVal: toString(i),
      }
      myDeck.push(card);
    }
    //create jack, queen,king
    for (j = 0; j < 3; j++) {
      const card = {
        suit: cardSuit,
        val: 10,
        displayVal: faceCards[j],
      }
      myDeck.push(card);
    }
    //create ace for each suit
    const card = {
      suit: cardSuit,
      val: 11,
      displayVal: 'Ace',
    }
    myDeck.push(card);
  }
  return myDeck;
};



// CHECKS
const deck = getDeck();
console.log(`Deck length equals 52? ${deck.length === 52}`);

const randomCard = deck[Math.floor(Math.random() * 52)];

const cardHasVal = randomCard && randomCard.val && typeof randomCard.val === 'number';
console.log(`Random card has val? ${cardHasVal}`);

const cardHasSuit = randomCard && randomCard.suit && typeof randomCard.suit === 'string';
console.log(`Random card has suit? ${cardHasSuit}`);

const cardHasDisplayVal = randomCard &&
  randomCard.displayVal &&
  typeof randomCard.displayVal === 'string';
console.log(`Random card has display value? ${cardHasDisplayVal}`);