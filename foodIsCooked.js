/**
 * Determines whether meat temperature is high enough
 * @param {string} kind 
 * @param {number} internalTemp 
 * @param {string} doneness
 * @returns {boolean} isCooked
 */
const foodIsCooked = function(kind, internalTemp, doneness) {
  // Write function HERE

  if (kind === 'chicken') {     //chicken should be over 165

    return (internalTemp > 165);
      
  } else if (kind === 'beef' && doneness === 'rare') {    //rare beef should be between 125 and 135

    return (internalTemp >= 125 && internalTemp < 135);
    
  } else if (kind === 'beef' && doneness === 'medium') {  //medium beef should be between 135 and 155

    return (internalTemp >= 135 && internalTemp < 155);

  } else if (kind === 'beef' && doneness === 'well') {    //well beef should be above 155

    return (internalTemp >= 155); 
  }
};


// Test function
console.log(foodIsCooked('chicken', 90)); // should be false
console.log(foodIsCooked('chicken', 190)); // should be true
console.log(foodIsCooked('beef', 138, 'well')); // should be false
console.log(foodIsCooked('beef', 138, 'medium')); // should be true
console.log(foodIsCooked('beef', 138, 'rare')); // should be true